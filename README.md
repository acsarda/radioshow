# radioshow

This project is to record my favourite argentinian radio, https://undinamo.com/, since usually I cannot listen to them live. Also, this project is a great excuse to learn different technology stuff I am yet not familiar with.

_Un dinamo_ broadcasts the following shows:
- Hagan correr la voz (every day show).
- Almacen de discos (every day show).
- Yo te necesito drogado (occasional show).
- El esquizo (occasional show).

## Tasks
- [ ] How to record live audio stream?
- [ ] Activate the recording during the radio time.
- [ ] Process the record audio file:
  - [ ] Delete music sections for copyright issues. 
- [ ] Develop a web with an audio player.
- [ ] Integrate twitts from the radio show community.
